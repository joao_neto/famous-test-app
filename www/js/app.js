angular.module('app', ['famous.angular', 'ngTouch', 'ui.router'])

.run(function ($famous) {
  var FastClick = $famous['famous/inputs/FastClick'];
})

.controller('AppCtrl', function ($scope, $sidemenu, $page) {
  $scope.$sidemenu = $sidemenu;
  $scope.$page = $page;

  $scope.layoutOptions = {
    headerSize: 44,
    footerSize: 44
  };

  $scope.headerOptions = {
    classes: ['red-bg'],
    size: [undefined, 44],
    properties: {
      color: '#fff',
      lineHeight: '44px'
    }
  };

  $scope.footerOptions = {
    classes: ['red-bg'],
    size: [undefined, 44],
    properties: {
      lineHeight: '44px',
      textAlign: 'center'
    }
  };
})

.controller('SideMenuCtrl', function ($scope) {
  $scope.items = [
    {
      title: 'Dashboard',
      uiSref: '.dashboard',
      options: {
        properties: {
          lineHeight: '44px',
          textAlign: 'center'
        }
      }
    },
    {
      title: 'State2',
      uiSref: '.state2',
      options: {
        properties: {
          lineHeight: '44px',
          textAlign: 'center'
        }
      }
    }
  ];
})

.controller('DashboardCtrl', function ($scope) {
  $scope.$page.title = 'Dashboard';
  $scope.contentOptions = {
    properties: {
      backgroundColor: '#fafaaa'
    }
  }
})

.controller('State2Ctrl', function ($scope) {
  $scope.$page.title = 'State2';
  $scope.contentOptions = {
    properties: {
      backgroundColor: '#c0c0c0'
    }
  }
})

.config(function ($stateProvider, $urlRouterProvider) {
  /*
  $stateProvider.decorator('views', function (state, parent) {
    var result = {};
    var views = parent(state);

    console.log(state)

    // angular.forEach(views, function(config, name) {
    //   var autoName = (state.name + '.' + name).replace('.', '/');
    //   config.templateUrl = config.templateUrl || "/partials/" + autoName + ".html";
    //   result[name] = config;
    // });
    return views;
  });
  */

  $stateProvider
    .state('app', {
      abstract: true,
      views: {
        'app': {
          controller: 'AppCtrl',
          templateUrl: 'templates/app-default.html'
        },
        'sidemenu@app': {
          controller: 'SideMenuCtrl',
          templateUrl: 'templates/side-menu.html'
        },
        'header@app': {
          templateUrl: 'templates/header.html'
        },
        // 'footer@app': {
        //   templateUrl: 'templates/footer.html'
        // },
      }
    })
    .state('app.dashboard', {
      url: '/',
      views: {
        'content': {
          controller: 'DashboardCtrl',
          templateUrl: 'templates/dashboard.html'
        }
      }
    })
    .state('app.state2', {
      url: '/state2',
      views: {
        'content': {
          controller: 'State2Ctrl',
          templateUrl: 'templates/state2.html'
        }
      }
    })

  $urlRouterProvider
    .when('', '/')
    .otherwise('/');
})
