var gulp = require('gulp');
var gutil = require('gulp-util');
var shell = require('gulp-shell');
var download = require('gulp-download');
var unzip = require('gulp-unzip');
var clean = require('gulp-clean');
var runSequence = require('gulp-run-sequence');
var fs = require('fs');
var path = require('path');

// https://crosswalk-project.org/#documentation/cordova/migrate_an_application

var config = {
  CACHE_DIR: 'cache',
  CORDOVA_LIB_DIR: path.join('platforms', 'android', 'CordovaLib'),
  CROSSWALK_ARCH: 'arm',
  CROSSWALK_VERSION: '8.37.189.12',
  CROSSWALK_CDN_BASEURL: 'https://download.01.org/crosswalk/releases/crosswalk/android/stable',
  get CROSSWALK_CORDOVA_PKG() {
    return ['crosswalk-cordova-', this.CROSSWALK_VERSION, '-', this.CROSSWALK_ARCH].join('');
  },
  get CROSSWALK_CORDOVA_FILENAME() {
    return [this.CROSSWALK_CORDOVA_PKG, '.zip'].join('');
  },
  get CROSSWALK_CORDOVA_DOWNLOAD_URL() {
    return [
      this.CROSSWALK_CDN_BASEURL,
      this.CROSSWALK_VERSION,
      this.CROSSWALK_ARCH,
      this.CROSSWALK_CORDOVA_FILENAME
    ].join('/')
  }
};


/**
 * Clean
 */
gulp.task('crosswalk:clean', function (done) {
  runSequence([
    'crosswalk:clean:version',
    'crosswalk:clean:cordovalib'
  ], done);
});

gulp.task('crosswalk:clean:version', function () {
  gulp.src(config.CORDOVA_LIB_DIR).pipe(clean());
});

gulp.task('crosswalk:clean:cordovalib', function () {
  gulp.src(config.CORDOVA_LIB_DIR).pipe(clean({force: true}));
});

/**
 * Copy
 */
gulp.task('crosswalk:copy', function (done) {
  runSequence([
    'crosswalk:copy:versionfile',
    'crosswalk:copy:cordovalib'
  ], done);
});

gulp.task('crosswalk:copy:versionfile', function () {
  var cache_dir = path.join(config.CACHE_DIR, 'crosswalk');
  var cache_pkg_file = path.join(cache_dir, config.CROSSWALK_CORDOVA_PKG, 'VERSION');
  var dest = path.join('platforms', 'android');
  gulp.src(cache_pkg_file).pipe(gulp.dest(dest));
});

gulp.task('crosswalk:copy:cordovalib', function () {
  var cache_dir = path.join(config.CACHE_DIR, 'crosswalk');
  var cache_pkg_dir = path.join(cache_dir, config.CROSSWALK_CORDOVA_PKG, 'framework');
  gulp.src(path.join(cache_pkg_dir, '**', '*')).pipe(gulp.dest(config.CORDOVA_LIB_DIR))
});

/**
 * Install
 */
gulp.task('install', [
  'bower:install',
  'cordova:install',
  'crosswalk:install'
]);

gulp.task('bower:install', shell.task([
  'bower install'
]));

gulp.task('cordova:install', shell.task([
  'cordova plugin add org.apache.cordova.inappbrowser',
  'cordova plugin add https://github.com/oauth-io/oauth-phonegap'
]));

gulp.task('crosswalk:install', function (done) {
  runSequence(
    'crosswalk:clean',
    'crosswalk:compile',
    'crosswalk:copy',
    done);
});

/**
 * Compile
 */
gulp.task('crosswalk:compile', function (done) {
  runSequence(
    'crosswalk:compile:cordova',
    'crosswalk:compile:webview',
    done);
});

gulp.task('crosswalk:compile:cordova', ['crosswalk:download', 'crosswalk:compile:webview'], function () {
  var cache_dir = path.join(config.CACHE_DIR, 'crosswalk');
  var cache_pkg_dir = path.join(cache_dir, config.CROSSWALK_CORDOVA_PKG, 'framework');

  // @todo make target and ant debug or ant release parametrizable
  return shell.task([
    'android update project --subprojects --path . --target "android-19"',
    'ant release'
  ], { cwd: cache_pkg_dir }).call();
});

gulp.task('crosswalk:compile:webview', ['crosswalk:download'], function () {
  var cache_dir = path.join(config.CACHE_DIR, 'crosswalk');
  var cache_pkg_dir = path.join(cache_dir, config.CROSSWALK_CORDOVA_PKG, 'framework', 'xwalk_core_library');

  // @todo make target and ant debug or ant release parametrizable
  return shell.task([
    'android update project --subprojects --path . --target "android-19"',
    'ant release'
  ], { cwd: cache_pkg_dir }).call();
});

/**
 * Download
 */
gulp.task('crosswalk:download', function (done) {
  var cache_dir = path.join(config.CACHE_DIR, 'crosswalk');
  var cache_pkg_dir = path.join(cache_dir, config.CROSSWALK_CORDOVA_PKG);
  var cache_pkg_file = path.join(cache_dir, config.CROSSWALK_CORDOVA_FILENAME);
  var url = config.CROSSWALK_CORDOVA_DOWNLOAD_URL;

  fs.exists(cache_pkg_dir, function (exists) {
    if (exists) return done();

    download(url)
      .pipe(gulp.dest(cache_dir))
      .on('error', done)
      .on('end', function () {
        gulp.src(cache_pkg_file)
          .pipe(unzip())
          .pipe(gulp.dest(cache_dir))
          .on('error', done)
          .on('end', function () {
            gulp.src(cache_pkg_file)
              .pipe(clean())
              .on('data', function() {})
              .on('error', done)
              .on('end', done);
          });
      });
  });
});

/**
 * Platforms
 */
gulp.task('cordova:add:android', [
  'cordova:platforms:add:android',
  'cordova:install'
]);

gulp.task('cordova:add:ios', [
  'cordova:platforms:add:android',
  'cordova:plugins:install'
]);

gulp.task('cordova:platforms:add:android', shell.task([
  'cordova platforms add android'
]));

gulp.task('cordova:platforms:add:ios', shell.task([
  'cordova platforms add ios'
]));

/**
 * Build
 */
gulp.task('build', shell.task([
  'cordova build'
]));

